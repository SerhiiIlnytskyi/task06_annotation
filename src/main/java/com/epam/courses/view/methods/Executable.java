package com.epam.courses.view.methods;

@FunctionalInterface
public interface Executable {
  void execute();
}
